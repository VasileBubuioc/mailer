
## Description
These are required points: Authentication and Authorization and CRUD for Emails.I skipped the optional part but if i will implement it i will do with websockets.

## Prerequisites
To run this project you must have Docker installed to run the DB container.
Must create a .env file in the root directory.
Can use this data for local development:
```
# app
PORT=3000

#postgres
POSTGRES_HOST=localhost
POSTGRES_DATABASE=mail
POSTGRES_USERNAME=mail
POSTGRES_PASSWORD=mail
POSTGRES_PORT=5436

# jwt
ACCESS_SECRET=superPuperSecret
ACCESS_EXPIRE=36000

```
To run docker container use:
```bash
cd devops/

docker compose --env-file ../.env -f docker-compose.local.yml up -d
 ```
## Installation

```bash
$ yarn install
```

## Running the app
```
$ yarn run start:dev
```

