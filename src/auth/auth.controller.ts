import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { SignUpDto } from './dto/sign.up.dto';
import { SignInDto } from './dto/sign.in.dto';
import { JwtResponseDto } from './dto/jwt.response.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @ApiOperation({
    description: 'Create new account',
  })
  @ApiCreatedResponse({
    description: 'Successfully created new account',
  })
  @ApiBadRequestResponse({
    description: 'Email already taken',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('sign-up')
  async signUp(@Body() signUpDto: SignUpDto): Promise<void> {
    return this.authService.signUp(signUpDto);
  }

  @ApiOperation({
    description: 'Sign into account',
  })
  @ApiCreatedResponse({
    description: 'Successfully sign into account',
  })
  @ApiBadRequestResponse({
    description: 'Wrong credentials',
  })
  @HttpCode(HttpStatus.CREATED)
  @Post('sign-in')
  async signIn(@Body() singInDto: SignInDto): Promise<JwtResponseDto> {
    return this.authService.signIn(singInDto);
  }
}
