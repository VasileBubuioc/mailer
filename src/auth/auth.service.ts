import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersEntity } from 'src/users/entity/users.entity';
import { Repository } from 'typeorm';
import { SignUpDto } from './dto/sign.up.dto';
import { UsersDto } from 'src/users/dto/users.dto';
import * as bcrypt from 'bcrypt';
import { UsersService } from 'src/users/users.service';
import { SignInDto } from './dto/sign.in.dto';
import { JwtService } from '@nestjs/jwt';
import { IUsersPayload } from 'src/common/interfaces/users.payload.interface';
import { JwtResponseDto } from './dto/jwt.response.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(signUpDto: SignUpDto): Promise<void> {
    const user: UsersDto = await this.usersRepository.findOneBy({
      email: signUpDto.email,
    });

    if (user) {
      throw new BadRequestException(
        `Email ${signUpDto.email} is already taken`,
      );
    }

    signUpDto.password = await bcrypt.hash(signUpDto.password, 10);
    await this.usersService.createOne(signUpDto);

    return void 0;
  }

  async signIn(signInDto: SignInDto): Promise<JwtResponseDto> {
    const user = await this.usersRepository.findOne({
      where: { email: signInDto.email },
      select: ['email', 'id', 'password'],
    });
    if (!user) {
      throw new BadRequestException('Wrong credentials');
    }

    const passwordMatch: boolean = await bcrypt.compare(
      signInDto.password,
      user.password,
    );
    if (!passwordMatch) {
      throw new BadRequestException('Wrong credentials');
    }
    const payload: IUsersPayload = { id: user.id, email: user.email };

    const token = await this.jwtService.signAsync(payload, {
      secret: 'superPuperSecret',
      expiresIn: '3600s',
    });

    return { accessToken: token };
  }
}
