import { ApiProperty } from '@nestjs/swagger';

export class JwtResponseDto {
  @ApiProperty({
    description: 'Access token for authorization',
  })
  accessToken: string;
}
