import { UsersEntity } from 'src/users/entity/users.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('mails')
export class MailsEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: String,
    name: 'theme',
  })
  theme: string;

  @Column({
    type: String,
    name: 'message',
  })
  message: string;

  @ManyToOne(() => UsersEntity, (user) => user.sentMails)
  @JoinColumn({ name: 'sedner_id' })
  sender: UsersEntity;

  @ManyToOne(() => UsersEntity, (user) => user.recivedMails)
  @JoinColumn({ name: 'reciver_id' })
  reciver: UsersEntity;
}
