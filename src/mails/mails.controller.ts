import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { MailsService } from './mails.service';
import { SendMailDto } from './dto/send.mail.dto';
import { User } from 'src/common/decorators/user.decorator';
import { IUsersPayload } from 'src/common/interfaces/users.payload.interface';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt/jwt.auth.guard';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { MailsDto } from './dto/mails.dto';
import { UpdateMailsDto } from './dto/update.mails.dto';

@ApiTags('Mails')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('mails')
export class MailsController {
  constructor(private readonly mailsService: MailsService) {}

  @ApiOperation({
    description: 'Send an email',
  })
  @ApiCreatedResponse({
    description: 'Email was sent',
  })
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.CREATED)
  @Post('send')
  async sendMail(
    @Body() sendMailDto: SendMailDto,
    @User() user: IUsersPayload,
  ): Promise<void> {
    return this.mailsService.sendEmail(sendMailDto, user);
  }

  @ApiOperation({
    description: 'Get sent emails',
  })
  @ApiOkResponse({
    description: 'Get sent emails',
  })
  @HttpCode(HttpStatus.OK)
  @Get('sent')
  async getSentMails(
    @Query('take') take: number,
    @Query('skip') skip: number,
    @User() user: IUsersPayload,
  ): Promise<MailsDto[]> {
    const paginationDto = new PaginationDto(take, skip);
    return this.mailsService.getSentMails(user, paginationDto);
  }

  @ApiOperation({
    description: 'Get recived emails',
  })
  @ApiOkResponse({
    description: 'Get recived emails',
  })
  @HttpCode(HttpStatus.OK)
  @Get('recived')
  async getRecivedMails(
    @Query('take') take: number,
    @Query('skip') skip: number,
    @User() user: IUsersPayload,
  ): Promise<MailsDto[]> {
    const paginationDto = new PaginationDto(take, skip);
    return this.mailsService.getRecivedMails(user, paginationDto);
  }

  @ApiOperation({
    description: 'Update one mail',
  })
  @ApiCreatedResponse({
    description: 'Successfuly updated',
  })
  @ApiNotFoundResponse()
  @Patch(':id')
  async updateOne(
    @Param('id') id: string,
    @Body() updateMailsDto: UpdateMailsDto,
    @User() user: IUsersPayload,
  ): Promise<void> {
    return this.mailsService.updateOne(id, user, updateMailsDto);
  }

  @ApiOperation({
    description: 'Delete one mail',
  })
  @ApiCreatedResponse({
    description: 'Successfuly deleted',
  })
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.CREATED)
  @Delete(':id')
  async deleteOne(
    @Param('id') id: string,
    @User() user: IUsersPayload,
  ): Promise<void> {
    return this.mailsService.deleteOne(id, user);
  }
}
