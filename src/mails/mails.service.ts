import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MailsEntity } from './entity/mails.entity';
import { Repository } from 'typeorm';
import { SendMailDto } from './dto/send.mail.dto';
import { IUsersPayload } from 'src/common/interfaces/users.payload.interface';
import { UsersService } from 'src/users/users.service';
import { UsersDto } from 'src/users/dto/users.dto';
import { MailsDto } from './dto/mails.dto';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { UpdateMailsDto } from './dto/update.mails.dto';

@Injectable()
export class MailsService {
  constructor(
    @InjectRepository(MailsEntity)
    private readonly mailsRepository: Repository<MailsEntity>,
    private readonly usersService: UsersService,
  ) {}

  async sendEmail(
    sendMailDto: SendMailDto,
    user: IUsersPayload,
  ): Promise<void> {
    const sender: UsersDto = await this.usersService.findOneWithoutRelations(
      user.email,
    );
    const reciver: UsersDto = await this.usersService.findOneWithoutRelations(
      sendMailDto.reciverEmail,
    );
    const newMail: MailsDto = this.mailsRepository.create({
      ...sendMailDto,
      reciver,
      sender,
    });

    await this.mailsRepository.save(newMail).catch((err) => {
      throw err;
    });

    return void 0;
  }

  async getSentMails(
    user: IUsersPayload,
    paginationDto: PaginationDto,
  ): Promise<MailsDto[]> {
    return this.mailsRepository
      .createQueryBuilder('mails')
      .leftJoin('mails.sender', 'sender')
      .leftJoinAndSelect('mails.reciver', 'reciver')
      .where('sender.id = :id', { id: user.id })
      .take(paginationDto.take)
      .skip(paginationDto.skip)
      .getMany()
      .catch((error) => {
        throw error;
      });
  }

  async getRecivedMails(
    user: IUsersPayload,
    paginationDto: PaginationDto,
  ): Promise<MailsDto[]> {
    return this.mailsRepository
      .createQueryBuilder('mails')
      .leftJoin('mails.reciver', 'reciver')
      .leftJoinAndSelect('mails.sender', 'sender')
      .where('reciver.id = :id', { id: user.id })
      .take(paginationDto.take)
      .skip(paginationDto.skip)
      .getMany()
      .catch((error) => {
        throw error;
      });
  }

  async updateOne(
    id: string,
    user: IUsersPayload,
    updateMailsDto: UpdateMailsDto,
  ): Promise<void> {
    await this.getOne(id, user);

    const updateResult = await this.mailsRepository
      .createQueryBuilder('mail')
      .update(MailsEntity)
      .set(updateMailsDto)
      .where('id = :id', { id })
      .execute()
      .catch((error) => {
        throw error;
      });
    if (updateResult.affected === 0) {
      throw new InternalServerErrorException(`Cannot update mail ${id}`);
    }

    return void 0;
  }

  async deleteOne(id: string, user: IUsersPayload): Promise<void> {
    await this.getOne(id, user);
    const deleteResult = await this.mailsRepository
      .createQueryBuilder()
      .delete()
      .from(MailsEntity)
      .where('id = :id', { id })
      .execute()
      .catch((error) => {
        throw error;
      });

    if (deleteResult.affected === 0) {
      throw new InternalServerErrorException(`Cannot delete mail ${id}`);
    }

    return void 0;
  }

  private async getOne(id: string, user: IUsersPayload): Promise<MailsDto> {
    const mail = await this.mailsRepository
      .createQueryBuilder('mail')
      .leftJoinAndSelect('mail.sender', 'sender')
      .where('mail.id = :id', { id })
      .andWhere('sender.id = :senderId', { senderId: user.id })
      .getOne()
      .catch((error) => {
        throw error;
      });
    if (!mail) {
      throw new NotFoundException(`Mail ${id} not found`);
    }

    return mail;
  }
}
