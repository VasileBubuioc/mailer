import { Module } from '@nestjs/common';
import { MailsController } from './mails.controller';
import { MailsService } from './mails.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailsEntity } from './entity/mails.entity';
import { UsersService } from 'src/users/users.service';
import { UsersEntity } from 'src/users/entity/users.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MailsEntity, UsersEntity])],
  controllers: [MailsController],
  providers: [MailsService, UsersService],
})
export class MailsModule {}
