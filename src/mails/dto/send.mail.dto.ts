import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SendMailDto {
  @ApiProperty({
    type: String,
    name: 'theme',
    example: 'Invitation',
  })
  @IsString()
  @IsNotEmpty()
  theme: string;

  @ApiProperty({
    type: String,
    name: 'message',
    example: 'Hi my dear friend, I want to invite you...',
  })
  @IsString()
  @IsNotEmpty()
  message: string;

  @ApiProperty({
    type: String,
    name: 'reciverEmail',
    example: 'reciver@mail.com',
  })
  @IsEmail()
  @IsNotEmpty()
  reciverEmail: string;
}
