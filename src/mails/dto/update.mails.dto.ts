import { OmitType } from '@nestjs/swagger';
import { SendMailDto } from './send.mail.dto';

export class UpdateMailsDto extends OmitType(SendMailDto, [
  'reciverEmail',
] as const) {}
