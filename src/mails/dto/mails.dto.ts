import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { UsersDto } from 'src/users/dto/users.dto';

export class MailsDto {
  @ApiProperty({
    type: String,
  })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({
    type: String,
    name: 'theme',
    example: 'Invitation',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  theme: string;

  @ApiProperty({
    type: String,
    name: 'message',
    example: 'Hi my dear friend, I want to invite you...',
  })
  @IsString()
  @IsNotEmpty()
  @Expose()
  message: string;

  @ApiProperty({
    type: UsersDto,
    name: 'sender',
  })
  @Expose()
  @Type(() => UsersDto)
  sender: UsersDto;

  @ApiProperty({
    type: UsersDto,
    name: 'reciver',
  })
  @Expose()
  @Type(() => UsersDto)
  reciver: UsersDto;
}
