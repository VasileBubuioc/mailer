export const configuration = () => ({
  port: process.env.PORT,
  postgresConfig: {
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DATABASE,
    username: process.env.POSTGRES_USERNAME,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
  },
  jwtConfig: {
    secret: process.env.ACCESS_SECRET,
    expire: process.env.ACCESS_EXPIRE,
  },
});
