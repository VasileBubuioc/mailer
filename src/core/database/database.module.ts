import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IPostgresConfig } from 'src/common/interfaces/postgres.config.interface';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        const databaseConfig =
          configService.get<IPostgresConfig>('postgresConfig');
        return {
          type: 'postgres',
          host: databaseConfig.host,
          database: databaseConfig.database,
          username: databaseConfig.username,
          password: databaseConfig.password,
          port: databaseConfig.port,
          autoLoadEntities: true,
          synchronize: true,
          logging: false,
        };
      },
    }),
  ],
})
export class DatabaseModule {}
