import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';
import { MailsDto } from 'src/mails/dto/mails.dto';

export class UsersDto {
  @ApiProperty({
    description: "User's id",
  })
  @IsUUID()
  @Expose()
  id: string;

  @ApiProperty({
    description: "User's email",
  })
  @IsEmail()
  @Expose()
  email: string;

  @ApiProperty({
    description: "User's password",
  })
  @IsString()
  @IsNotEmpty()
  password: string;

  @ApiPropertyOptional({
    type: MailsDto,
    isArray: true,
    name: 'sentMails',
  })
  @Type(() => MailsDto)
  @IsArray()
  @IsOptional()
  @ValidateNested({ each: true })
  @Expose()
  sentMails: MailsDto[];

  @ApiPropertyOptional({
    type: MailsDto,
    isArray: true,
    name: 'recivedMails',
  })
  @Type(() => MailsDto)
  @IsArray()
  @IsOptional()
  @ValidateNested({ each: true })
  @Expose()
  recivedMails: MailsDto[];
}
