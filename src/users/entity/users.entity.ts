import { MailsEntity } from 'src/mails/entity/mails.entity';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('users')
export class UsersEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: String,
    unique: true,
    name: 'email',
  })
  @Index()
  email: string;

  @Column({
    type: String,
    name: 'password',
    select: false,
  })
  password: string;

  @OneToMany(() => MailsEntity, (mail) => mail.sender)
  @JoinColumn({ name: 'sent_mails' })
  sentMails: MailsEntity[];

  @OneToMany(() => MailsEntity, (mail) => mail.reciver)
  @JoinColumn({ name: 'recived_mails' })
  recivedMails: MailsEntity[];
}
