import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersEntity } from './entity/users.entity';
import { Repository } from 'typeorm';
import { SignUpDto } from 'src/auth/dto/sign.up.dto';
import { UsersDto } from './dto/users.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,
  ) {}

  async createOne(signUpDto: SignUpDto): Promise<void> {
    try {
      const newUser = this.usersRepository.create(signUpDto);
      await this.usersRepository.save(newUser);

      return void 0;
    } catch (error) {
      if (error.code === '23505') {
        throw new BadRequestException(
          `Email ${signUpDto.email} is already taken`,
        );
      }

      throw error;
    }
  }

  async findOneWithoutRelations(email: string): Promise<UsersDto> {
    const user: UsersDto = await this.usersRepository.findOneBy({ email });
    if (!user) {
      throw new NotFoundException(`User ${email} not found`);
    }

    return user;
  }
}
