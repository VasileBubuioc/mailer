export class PaginationDto {
  constructor(take: number, skip: number) {
    (this.take = take), (this.skip = skip);
  }
  take: number;
  skip: number;
}
