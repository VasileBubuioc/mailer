export interface IJwtConfig {
  secret: string;
  expire: number;
}
