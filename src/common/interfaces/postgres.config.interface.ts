export interface IPostgresConfig {
  host: string;
  database: string;
  username: string;
  password: string;
  port: number;
}
