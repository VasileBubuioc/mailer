export interface IUsersPayload {
  id: string;
  email: string;
}
